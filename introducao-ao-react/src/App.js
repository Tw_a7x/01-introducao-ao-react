import React, { Component } from 'react';
import './App.css';

class App extends Component {
    render() {
        const d= new Date();
        return (
          <div className="App">
            <h1> Hello World</h1>
            <p>{d.toString()}</p>
            <p className="cor">Hello World</p>
            <p>{d.toString()}</p>
            <p className="cor2">Hello World</p>
            <p>{d.toString()}</p>
            <p className="cor3">hello world <br></br>{d.toString()}</p>
            <p className="cor4">Hello World Brasil</p>
            <p>{d.toString()}</p>
            <p className="cor5">Hello World <br></br>{d.toString()}</p>
            <p className="cor6">Hello World <br></br>{d.toString()}</p>
            <p className="cor7">H3ll0 W0rl4 <br></br>{d.toString()}</p>
            <p className="cor8">hello WORLD </p>
            <p>{d.toString()}</p>
            <p className="cor9">Hello World </p>
            <p>{d.toString()}</p>
      </div>
    );
  }
}

export default App;
